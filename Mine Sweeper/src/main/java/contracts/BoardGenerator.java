package contracts;

public interface BoardGenerator {

    String[][] showBoard(Difficulties difficulty);

}
